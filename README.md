# rc

Dotfiles that configure my things.

# How do I get this to work?

Symlink the folders in this repo to the appropriate places.
For example, run `ln -s nvim/ ~/.config/nvim` to link the neovim configuration.

# Where do the things go?

I should probably write a bash script for this...

| file / directory | target directory |
| ---------------- | ---------------- |
| .flake8          | home             |
| .tmux.conf       | home             |
| nvim/            | home/.config     |
