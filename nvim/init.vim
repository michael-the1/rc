syntax enable
filetype plugin on
filetype indent on

set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab
set shiftround
set smarttab

set number relativenumber   " set hybrid (absoulte/relative) linenumbers
set ruler                   " show cursor position at all times

" https://kinbiko.com/vim/my-shiniest-vim-gems/
set listchars=tab:>-,trail:·    " Make whitespace visible
set list
set cursorcolumn    " highlight current column
match ErrorMsg '\%>120v.\+'
match ErrorMsg '\s\+$'

call plug#begin('~/.config/nvim/plugged')
Plug 'nvie/vim-flake8'
Plug 'altercation/vim-colors-solarized'
Plug 'davidhalter/jedi-vim'
Plug 'gabrielelana/vim-markdown'
Plug 'scrooloose/nerdtree'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
Plug 'w0rp/ale'
call plug#end()

" Have to call this after plugins
let g:solarized_termcolors=256
set background=dark
colorscheme solarized

" Disable spelling checks in markdown files
let g:markdown_enable_spell_checking = 0

" Ctrl + o to toggle NERDTree
map <C-o> :NERDTreeToggle<CR>
